//
//  PlayingCardDeck.swift
//  PlayingCard
//
//  Created by XeVoNx on 04/04/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import Foundation

struct PlayingCardDeck
{
    private(set) var cards = [PlayingCard]()
    
    //init full deck
    init() {
        //name is changed to reflect the nesting of types
        for suit in PlayingCard.Suit.all { //use the static vars
            for rank in PlayingCard.Rank.all {
                cards.append(PlayingCard(suit: suit, rank: rank))
            }
        }
    }
    
    //struct is value type, mark func with mutating
    mutating func draw() -> PlayingCard? {
        if cards.count > 0 {
            return cards.remove(at: cards.count.myRand)
        } else { //if i dont have any cards left
            return nil
        }
    }
}

extension Int {
    //can only have computed properties
    var myRand: Int {
        if self > 0 {
            return Int.random(in: 0 ..< self) //self is the int sent
        } else if self < 0 {
            return -Int.random(in: 0 ..< self)
        } else {
            return 0
        }
       
    }
}
