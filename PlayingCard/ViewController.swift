//
//  ViewController.swift
//  PlayingCard
//
//  Created by XeVoNx on 03/04/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var deck = PlayingCardDeck()
    
    @IBOutlet weak var playingCardView: PlayingCardView! {
        
        didSet {
            
            //swipe flips through cards-> affects model -> handled by this (self) ViewController controller (the view does not touch the model)
            let swipe = UISwipeGestureRecognizer(target: self, action: #selector(nextCard))
            swipe.direction = [.left,.right]
            playingCardView.addGestureRecognizer(swipe)
            
            //This is handlde by the view (playingCardView: PlayingCardView) //only view changes
            let pinchSelector = #selector(PlayingCardView.adjustFaceCardScale(byHandlingGestureRecognizerBy:))
            let pinch = UIPinchGestureRecognizer(target: playingCardView, action: pinchSelector)
            playingCardView.addGestureRecognizer(pinch)
        }
        
    }
    
    @IBAction func flipCard(_ sender: UITapGestureRecognizer) {
    //Added in IB: + tapGestureRec, above screen drag from tapGestureRec to this view, change outlet to action, any to gestureRecognizer
        
        //switch on recognizer state
        switch sender.state {
        case .ended:
            playingCardView.isFaceUp = !playingCardView.isFaceUp
        
        default: break
        }
        
    }
    
    //export this method out of swift to the objc runtime
    @objc func nextCard() {
        
        //if let, deck might be empty
        if let card = deck.draw() {
            
            //Here the controller converts between the two(view<->model)
            playingCardView.rank = card.rank.order
            playingCardView.suit = card.suit.rawValue
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Testing printing
        for _ in 1...10 {
            if let card = deck.draw() { //optional - deck may be empty
                print("\(card)")
            }
        }
    }


}

