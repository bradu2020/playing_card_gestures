//
//  PlayingCard.swift
//  PlayingCard
//
//  Created by XeVoNx on 04/04/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

//BETTER WAY:
//Just make an Enum conform to protocol CaseIterable. Now you can call .allCases property of an Enum i.e. for index in SomeEnum.allCases{...doSmth...}

import Foundation

//mske this print nice implementing this protocol
struct PlayingCard: CustomStringConvertible{
    
    var description: String { return "\(rank) \(suit)"}
    
    //nested enums, makes sense only inside this PlayingCard struct

    var suit: Suit
    var rank: Rank

    // :Int  or :String to generate rawValue for backwards comp objc
    enum Suit: String, CustomStringConvertible {
        case spades = "♠️"
        case hearts = "♥️"
        case clubs = "♣️"
        case diamonds = "♦️"
        
        static var all: [Suit] {
            return [.spades, .hearts, .clubs, .diamonds]
        }
        var description: String { return self.rawValue }
    }
//    enum Suit: CustomStringConvertible{
//        var description: String {
//            switch self {
//            case .spades: return "of Spades"
//            case .hearts: return "of Hearts"
//            case .diamonds: return "of Diamonds"
//            case .clubs: return "of Clubs"
//            }
//        }
//
//        case spades
//        case hearts
//        case diamonds
//        case clubs
//        //static var- easier to get all the suits in a loop
//        static var all = [Suit.spades,.hearts,.diamonds,.clubs]
        
    
    
    //best have case for each one but for learning associated properties:
    enum Rank: CustomStringConvertible {
        var description: String {
        switch self {
        case .ace: return "A"
        case .numeric(let pips): return "\(pips)"
        case .face(let kind): return "\(kind)"
        }
            
        }
        
        case ace
        case face(String) //bad representation
        case numeric(Int)
    
        var order: Int { //should be Int?
            switch self {
            case .ace: return 1
            case .numeric(let pips): return pips
                
            //pattern matching
            case .face(let kind) where kind == "J": return 11
            case .face(let kind) where kind == "Q": return 12
            case .face(let kind) where kind == "K": return 13
            default: return 0 //bad this should return nil
            }
        }
        
        //static var- easier to get all the suits in a loop
        static var all: [Rank] {
            //var allRanks: [Rank] = [.ace] //cant infer this type because there could be anouter enum that has .ace case
            var allRanks = [Rank.ace]
            for pips in 2...10 { //including
                allRanks.append(Rank.numeric(pips))
            }
            allRanks += [Rank.face("J"),.face("Q"),.face("K")] //can infer if first elem has Rank.
            
            return allRanks
        }
    }

}
