//
//  PlayingCardView.swift
//  PlayingCard
//
//  Created by XeVoNx on 04/04/2020.
//  Copyright © 2020 XeVoNx. All rights reserved.
//

import UIKit

//Let IB know that it should render the view directly in the canvas, by default image names do not work -> in:,compatibleWith: Parameters
//myImg = UIImage(named: "imgName",in: Bundle(for: self.classForCoder), compatibleWith: traitCollection)
@IBDesignable
class PlayingCardView: UIView {
    
    //a stackview for example does not implement draw, other views are doing the drawing
    
    //Could use an NSAttributedString to draw corners in 5 lines of code in draw rect but now we are using 15 to use UILabel and learn how to build a view out of other views by making them SUBVIEWS:
    @IBInspectable //To change this from IB !!!IB CAN'T INFER TYPE!!!
    var rank: Int = 5 { didSet { setNeedsDisplay(); setNeedsLayout() } } //redraw this when changed //layout even if we dont use autoLayout
    @IBInspectable
    var suit: String = "♥️" { didSet { setNeedsDisplay(); setNeedsLayout() } } //one or both, always
    @IBInspectable
    var isFaceUp: Bool = true { didSet { setNeedsDisplay(); setNeedsLayout() } } //when you have public var
    
    //NO setNeedsLayout() because changing the (face)card size does not affect the corners
    var faceCardScale: CGFloat = SizeRatio.faceCardImageSizeToBoundsSize { didSet { setNeedsDisplay(); print(faceCardScale) } }
    //create a handler for pinch gesture
    @objc func adjustFaceCardScale(byHandlingGestureRecognizerBy recognizer: UIPinchGestureRecognizer) {
        switch recognizer.state {
        case .changed:
            faceCardScale *= recognizer.scale
        //to ensure incremental changes (otherwise would be exponential)
            recognizer.scale = 1.0
        
        //ignore all other states
        default: break
        }
    }
    
    
   private func centeredAttributedString(_ string: String, fontSize: CGFloat) -> NSAttributedString {
       var font = UIFont.preferredFont(forTextStyle: .body).withSize(fontSize) // caption, footnote, headline
        //TAKE INTO ACCOUNT THE SIZE FROM SETTINGS,SCALE BASED BY THIS SLIDER
       font = UIFontMetrics(forTextStyle: .body).scaledFont(for: font)
       let paragraphStyle = NSMutableParagraphStyle()
       paragraphStyle.alignment = .center
    
    return NSAttributedString(string: string, attributes: [ .paragraphStyle: paragraphStyle, .font:font]) //NSAttributedString.Key is inferred
   }
    
    
    private var cornerString: NSAttributedString {
        return centeredAttributedString(rankString+"\n"+suit, fontSize: cornerFontSize)
    }
    
    //until fully init, you can not call methods on self => mark with LAZY (init only when asked for)
    private lazy var upperLeftCornerLabel = createCornerLabel()
    private lazy var lowerRightCornerLabel = createCornerLabel()
    
    private func createCornerLabel() -> UILabel {
        let label = UILabel()
        
        label.numberOfLines = 0 //use as many lines as label needs
        addSubview(label) //add it as subview of self else its not drawing
        return label
    }
    
    private func configureCornerLabel(_ label: UILabel) { //best practice, name of func implies the external name
        label.attributedText = cornerString //our centered string with the right size depending on card size
        label.frame.size = CGSize.zero //if it already has width, it keeps its width => clear them before fit
        label.sizeToFit() //size the label to fit its contents
        label.isHidden = !isFaceUp //if not facedUp, dont draw these corner labels
    }
    
    //Next we need to position those labels dowRight, upLeft
    //when we call setNeedsLayout(), the sys will eventually call this method
    //or when bounds change, redraw subviews, call SUPER//
    
    
    //When text size slider changes in phone settings (or rotate,landscape,portrait,etc)
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        //reflect the expected changes in UI
        setNeedsDisplay()
        setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        configureCornerLabel(upperLeftCornerLabel)
        //bounds is where we draw, frame is where we position it
        upperLeftCornerLabel.frame.origin = bounds.origin.offsetBy(dx: cornerOffset, dy: cornerOffset)
        
        
        //A bit harder, needs to be rotated and offset:
        configureCornerLabel(lowerRightCornerLabel)
        //Turn it upside down => Every view has a .transform var that is a affineTransform (scale,translation,rotation)
        //1??) it nedes to be translated to its dowRight corner instead of upLeft //*OR NOT???*
        //2) rotate it around its source (up left that is now the old down right *OR NOT*) to be upside down
        lowerRightCornerLabel.transform = CGAffineTransform.identity //bitwise -> scaling this to be bigger -> pixelated
//            .translatedBy(x: lowerRightCornerLabel.frame.size.width, y: lowerRightCornerLabel.frame.size.height) //*OR NOT*
            .rotated(by: CGFloat.pi)
        //Another common methods: translate to center, rotate, translate it back
        
        lowerRightCornerLabel.frame.origin = CGPoint(x: bounds.maxX, y: bounds.maxY)
        //(because source is upperLeft point of the card): first move by the corner offset then the distance of the width and height of the card
            .offsetBy(dx: -cornerOffset, dy: -cornerOffset)
            .offsetBy(dx: -lowerRightCornerLabel.frame.width, dy: -lowerRightCornerLabel.frame.height)
    }
    
    
    // Draw numbers from pips
    private func drawPips()
    {
        let pipsPerRowForRank = [[0], [1], [1,1], [1,1,1], [2,2], [2,1,2], [2,2,2], [2,1,2,2], [2,2,2,2], [2,2,1,2,2], [2,2,2,2,2]]
        
        //embedded func that creates an attributed centered String (picks size by guessing what the right one should be then adjusting it)
        func createPipString(thatFits pipRect: CGRect) -> NSAttributedString {
            let maxVerticalPipCount = CGFloat(pipsPerRowForRank.reduce(0) { max($1.count, $0)})
            let maxHorizontalPipCount = CGFloat(pipsPerRowForRank.reduce(0) { max($1.max() ?? 0, $0)})
            let verticalPipRowSpacing = pipRect.size.height / maxVerticalPipCount
            let attemptedPipString = centeredAttributedString(suit, fontSize: verticalPipRowSpacing)
            let probablyOkayPipStringFontSize = verticalPipRowSpacing / (attemptedPipString.size().height / verticalPipRowSpacing)
            let probablyOkayPipString = centeredAttributedString(suit, fontSize: probablyOkayPipStringFontSize)
            if probablyOkayPipString.size().width > pipRect.size.width / maxHorizontalPipCount {
                return centeredAttributedString(suit, fontSize: probablyOkayPipStringFontSize /
                    (probablyOkayPipString.size().width / (pipRect.size.width / maxHorizontalPipCount)))
            } else {
                return probablyOkayPipString
            }
        }
        
        if pipsPerRowForRank.indices.contains(rank) {
            let pipsPerRow = pipsPerRowForRank[rank]
            var pipRect = bounds.insetBy(dx: cornerOffset, dy: cornerOffset).insetBy(dx: cornerString.size().width, dy: cornerString.size().height / 2)
            let pipString = createPipString(thatFits: pipRect)
            let pipRowSpacing = pipRect.size.height / CGFloat(pipsPerRow.count)
            pipRect.size.height = pipString.size().height
            pipRect.origin.y += (pipRowSpacing - pipRect.size.height) / 2
            for pipCount in pipsPerRow {
                switch pipCount {
                case 1:
                    pipString.draw(in: pipRect)
                case 2:
                    pipString.draw(in: pipRect.leftHalf)
                    pipString.draw(in: pipRect.rightHalf)
                default:
                    break
                }
                pipRect.origin.y += pipRowSpacing
            }
        }
    }
    
    
    //When we call setNeedsDisplay(), the sys will eventually call this method//
    //SET CONTENT MODE IN IB TO REDRAW TO CALL THIS EACH TIME BOUNDS CHANGE(ex: landscape view)
    override func draw(_ rect: CGRect) {
        
        //IB -> VIEW DRAWING -> CLEAR COLOR & UNTICK OPAQUE (whenever dealing with transparency)
        //Fill the entire bounds
        let roundedRect = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        roundedRect.addClip() //this will be the cliping area for all the drwaing (in case any code draws outside)
        UIColor.white.setFill()
        roundedRect.fill() //cant see the changes if OPAQUE is ticked
        
        if isFaceUp {
            
            //get img by name from assets,if it is faceCard
            if let faceCardImage = UIImage(named: rankString+suit,in: Bundle(for: self.classForCoder), compatibleWith: traitCollection) {
                
                //draw it, !full bounds it might smash into the corners! -> zoom down by a ratio
                faceCardImage.draw(in: bounds.zoom(by: faceCardScale)) //var because pinch gesture to zoom in/out

            } else {
                drawPips()
            }
            
        } else {
            if let cardBackImage = UIImage(named: "cardback",in: Bundle(for: self.classForCoder), compatibleWith: traitCollection) {
                //in the entire bounds, no corners to worry about
                cardBackImage.draw(in: bounds)
            }
        }
        
        
        
        /*Reminder:
        
//        //Can return nil in other contexts, not in draw rect
//        if let context = UIGraphicsGetCurrentContext() { //center of drawing area is defined by bounds
//            context.addArc(center: CGPoint(x: bounds.midX, y: bounds.midY), radius: 100.0, startAngle: 0, endAngle: 2*CGFloat.pi, clockwise: true) //angle of 0 means from the right L
//            context.setLineWidth(5.0)
//            UIColor.green.setFill()
//            UIColor.red.setStroke()
//            context.strokePath()
//            //context.fillPath() //does nothing because .strokePath already consumed the path, unlike with bezier path
//        }
        
        //
        
//        let path = UIBezierPath()
//        path.addArc(withCenter: CGPoint(x: bounds.midX, y: bounds.midY), radius: 100.0, startAngle: 0, endAngle: 2*CGFloat.pi, clockwise: true) //angle of 0 means from the right L
//        path.lineWidth = 5.0 //just a var,not a method
//        UIColor.green.setFill()
//        UIColor.red.setStroke()
//        path.stroke() //the difference is that the path still exists as an object
//        path.fill()
//        //or move the path,shrink it and fill it again etc
        */
    } //end of draw()

}

//constants
extension PlayingCardView {
    private struct SizeRatio {
        static let cornerFontSizeToBoundsHeight: CGFloat = 0.085
        static let cornerRadiusToBoundsHeight: CGFloat = 0.06
        static let cornerOffsetToCornerRadius: CGFloat = 0.33
        static let faceCardImageSizeToBoundsSize: CGFloat = 0.75
    }
    
    //computed properties!
    private var cornerRadius: CGFloat {
        return bounds.size.height * SizeRatio.cornerRadiusToBoundsHeight
    }
    private var cornerOffset: CGFloat {
        return cornerRadius * SizeRatio.cornerOffsetToCornerRadius
    }
    private var cornerFontSize: CGFloat {
        return bounds.size.height * SizeRatio.cornerFontSizeToBoundsHeight
    }
    private var rankString: String {
        switch rank {
        case 1: return "A"
        case 2...10: return String(rank)
        case 11: return "J"
        case 12: return "Q"
        case 13: return "K"
        default: return "?"
        }
    }
}

//to get right/left/etc easy
extension CGRect {
    var leftHalf: CGRect {
        return CGRect(x: minX, y: minY, width: width/2, height: height)
    }
    var rightHalf: CGRect {
        return CGRect(x: midX, y:minY, width: width/2, height: height)
    }
    func insert(by size: CGSize) -> CGRect { //CGSize
        return insetBy(dx: size.width, dy: size.height) //insetBy
    }
    func sized(to size: CGSize) -> CGRect {
        return CGRect(origin: origin, size: size)
    }
    func zoom(by scale: CGFloat) -> CGRect {
        let newWidth = width * scale
        let newHeight = height * scale
        return insetBy(dx: (width - newWidth), dy: (height - newHeight))
    }
}

extension CGPoint { //?
    func offsetBy(dx: CGFloat, dy: CGFloat) -> CGPoint {
        return CGPoint(x: x+dx, y: y+dy)
    }
}
